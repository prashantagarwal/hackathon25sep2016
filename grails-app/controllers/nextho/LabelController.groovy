package nextho


class LabelController {
    def springSecurityService


    def index() {
        User user = springSecurityService.currentUser
        render(view: '/label/index', model: [labels: Label.list(),user :user])

    }

    def createUpdateLabel() {

        if (params.url.equals("updateLabel")) {

            Label label = Label.findByUuid(params.uuid)
            label.name = params?.name
            label.save(flush: true)



        } else if (params.url.equals("createLabel")) {
            User user = springSecurityService.currentUser
            Label label = new Label(name: params?.name, createdBy: user).save(flush: true)
        }
        redirect(controller: 'label', action: 'index')
    }


    def deleteLabel() {
        Label label = Label.findByUuid(params.id)
        label.delete(flush: true)
        redirect(controller: 'label', action: 'index')
    }
}
