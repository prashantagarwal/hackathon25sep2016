package nextho

class UserController {

    def list = {
        [users: User.list(max:10,offset: 0), userInstanceTotal: User.count()]
    }
    def filter ={
        render(template:"/user/listTemplate" , model: [users: User.list(params), userInstanceTotal: User.count()])
    }
}
