package nextho

class IssuesController {

    def springSecurityService
    def notificationService


    def index() {
        render(view: '/issue/index', model: [issueList: Issue.list()])

    }

    def createUpdateIssue() {

        if (params.url.equals("updateIssue")) {

            Issue issue = Issue.findByUuid(params.uuid)
            issue.title = params?.name
            issue.save(flush: true)



        } else if (params.url.equals("createIssue")) {
            User user = springSecurityService.currentUser
            Issue issue = new Issue(name: params?.name, createdBy: user).save(flush: true)
            notificationService.ticketCreatedNotification(issue)
        }
        redirect(controller: 'issues', action: 'index')
    }


    def deleteIssue() {
        Issue issue = Issue.findByUuid(params.id)
        issue.delete(flush: true)
        redirect(controller: 'issues', action: 'index')
    }

    def issueDetail(){
        Issue issue = Issue.findByUuid(params.id)

        render view:'/issue/issueDetail'

    }
}
