package nextho

import grails.converters.JSON

class ProjectController {
    def springSecurityService
    def notificationService


    def index() {
        render(view: '/project/index', model: [projectList: Project.list()])

    }

    def create() {
        render(view: '/project/create', model: [teamList: Team.list(), issues: Issue.list(), labels: Label.list()])

    }

    def save() {
        println "=================params?.team" + params
        User user = springSecurityService.currentUser
        Project project = new Project()
        project.name = params.name
        Milestone milestone = new Milestone(name: params?.milestone, createdby: user, project: project)

        Team team = Team.findByUuid(params?.team)
      project.save(flush: true)
        if (team) {
            //team.save(flush: true)
            ProjectTeam projectTeam = new ProjectTeam()
            projectTeam.project=project
            projectTeam.team=team
            projectTeam.dateCreated=new Date()
            projectTeam.save(flush:true)
        }

        project.addToMilestones(milestone)
        milestone.save(flush: true)

        notificationService.projectCreatedNotification(user)
        redirect(controller: 'project', action: 'index')

    }


    def createUpdateProject() {

        if (params.url.equals("updateProject")) {

            Project project = Project.findByUuid(params.uuid)
            project.name = params?.name
            project.save(flush: true)


        } else if (params.url.equals("createProject")) {
            User user = springSecurityService.currentUser
            Project project = new Project(name: params?.name, createdBy: user).save(flush: true)
        }
        redirect(controller: 'project', action: 'index')
    }


    def deleteProject() {
        Project project = Project.findByUuid(params.id)
        project.delete(flush: true)
        redirect(controller: 'project', action: 'index')
    }

    def detail() {
        render(view: '/project/detail', model: [project: Project.findByUuid(params.id)])


    }

    def removeMilestone() {
        Map result = [:]
        Project project = Project.findByUuid(params.projectId)
        Milestone milestone = Milestone.findByUuid(params.id)
        project.removeFromMilestones(milestone)
        project.save(flush: true)
        result.template = g.render(template: '/project/milestones', model: [project: project])

        render result as JSON
    }

    def addMilestone() {
        User user = springSecurityService.currentUser
        Project project = Project.findByUuid(params.projectId)

        Milestone milestone = new Milestone(name: params?.milestone, createdby: user, project: project).save(flush: true)
        Map result = [:]
        project.addToMilestones(milestone)

        project.save(flush: true)
        result.template = g.render(template: '/project/milestones', model: [project: project])

        render result as JSON
    }

    def createIssue() {
        println "===========params" + params
        Issue issue = new Issue(title: params.title, user: User.findByUuid(params.user), description: params.description)
        issue.project = Project.findByUuid(params.projectUuid)
        issue.validate()
        issue.save(flush: true)
        println "===========" + issue.errors
        redirect(controller: 'project', action: 'index')
    }


}
