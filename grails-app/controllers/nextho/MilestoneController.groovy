package nextho

class MilestoneController {

    def springSecurityService


    def index() {
        render(view: '/milestone/index', model: [milestoneList: Milestone.list()])

    }

    def createUpdateMilestone() {

        if (params.url.equals("updateMilestone")) {

            Milestone milestone = Milestone.findByUuid(params.uuid)
            milestone.name = params?.name
            milestone.save(flush: true)



        } else if (params.url.equals("createIssue")) {
            User user = springSecurityService.currentUser
            Issue milestone = new Issue(name: params?.name, createdBy: user).save(flush: true)
        }
        redirect(controller: 'milestone', action: 'index')
    }


    def deleteIssue() {
        Issue milestone = Issue.findByUuid(params.id)
        milestone.delete(flush: true)
        redirect(controller: 'milestone', action: 'index')
    }
}
