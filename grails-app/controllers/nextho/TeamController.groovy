package nextho

import grails.converters.JSON

class TeamController {
    def springSecurityService

    def index() {
        User user = springSecurityService.currentUser
        println user?.id+"userid"
        render(view:"/team/index", model: [team:Team?.list(), user:user])
    }

    def addTeam() {
        User user=User.findAll()
        render user.username as JSON
    }
    def createUpdateTeam(){
        User user = springSecurityService.currentUser
        println params?.teamName
        String [] allUsers=params.allUsers.toString().split(",")
        Team team=new Team(name: params?.teamName, createdBy: user)
        for(String eachUser : allUsers){
            User u=User.findByEmailAddress(eachUser.trim())
            team.addToUser(u).save()
        }
        redirect(action: "index")
    }

}
