package nextho

class NotificationQueue {
    private static final long serialVersionUID = 1

    String emailTo
    String subject
    String content
    Boolean isRead
    Enums.NotificationQueueStatus status

    Date dateCreated
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()

    static constraints = {
        emailTo(nullable: true)
        subject(nullable: true)
        content(nullable: true)
        isRead(nullable: true)
        status(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        uuid(nullable: true)

    }
}
