package nextho

class Resource {
    private static final long serialVersionUID = 1


    String name
    String uuid=UUID.randomUUID()
    String resourcePath
    static constraints = {

        name nullable: true, blank:true
        uuid nullable: true, blank:true
        resourcePath nullable: true, blank : true
    }
}
