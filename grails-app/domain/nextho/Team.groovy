package nextho

class Team {
    private static final long serialVersionUID = 1

    String name
    Date dateCreated
    Date lastUpdated
    User createdBy
    String uuid = UUID.randomUUID().toString()
   static hasMany = [user: User]
    static constraints = {
        name(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        uuid(nullable: true)
        createdBy(nullable: true)
    }
}
