package nextho

class Milestone {
    private static final long serialVersionUID = 1

    String name
    User createdby
    String startDate
    String endDate
    Project project
    Date dateCreated= new Date()
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()
    static hasMany = [mileStoneTicket: MileStoneTicket]
    static constraints = {
        name(nullable: true)
        createdby(nullable: true)
        startDate(nullable: true)
        endDate(nullable: true)
        project(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
    }
}
