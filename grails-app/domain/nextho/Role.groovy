package nextho

class Role {
    private static final long serialVersionUID = 1

    String authority
    Date dateCreated
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()
    static mapping = {
        cache true
    }

    static constraints = {
        authority blank: false, unique: true
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        uuid(nullable: true)
    }
}
