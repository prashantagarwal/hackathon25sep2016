package nextho

class Comment {

    String uuid=UUID.randomUUID().toString()
    String comment
    User createdBy
    Date dateCreated=new Date()

    static constraints = {
        comment(nullable: true)
        createdBy(nullable: true)
        comment(nullable: true)
    }
}
