package nextho

class User {
	private static final long serialVersionUID = 1

	transient springSecurityService

	String username
	String password
	Boolean enabled=true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    String firstName
    String lastName
    String uuid=UUID.randomUUID().toString()
    String emailAddress

	static constraints = {
		username blank: false, unique: true
		password blank: false
	    firstName nullable: true,blank: true
        lastName nullable: true,blank: true
    }

	static mapping = {
		password column: '`password`'
	}

	static transients = ['fullName']



	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

	String getFullName(){
		"${this.firstName?:''} ${this.lastName?:''}"

	}
}
