package nextho

class Project {
    private static final long serialVersionUID = 1

    String name
    User createdBy
    Date dateCreated = new Date()
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()
    static hasMany = [issues: Issue,milestones: Milestone]
    static constraints = {
        name(nullable: true)
        createdBy(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
    }
}
