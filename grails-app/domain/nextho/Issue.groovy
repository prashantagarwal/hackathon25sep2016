package nextho

class Issue {
    private static final long serialVersionUID = 1
    String title
    Date dateCreated = new Date()
    Date lastUpdated
    String description

    String uuid = UUID.randomUUID().toString()
    User createdBy
    User user
    static hasMany = [labels: Label, resources: Resource, comments: Comment]
    static belongsTo = [project: Project]

    static constraints = {
        title(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        uuid(nullable: true)
        createdBy(nullable: true)
        user(nullable: true)
        description nullable: true, blank: true, maxSize: 5000
    }
}
