package nextho

class MileStoneTicket {
    private static final long serialVersionUID = 1


    Milestone milestone
    Issue issue
    Date dateCreated= new Date()
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()
    static constraints = {
        milestone(nullable: true)
        issue(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
    }
}
