package nextho

import nextho.User

class ProjectUser {
    private static final long serialVersionUID = 1

    Project project
    User user
    Enums.Access access = Enums.Access.READ
    Date dateCreated= new Date()
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()
    static hasMany = [milestone: Milestone]
    static constraints = {
        project(nullable: true)
        user(nullable: true)
        access(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
    }
}
