package nextho

class ProjectTeam {
    private static final long serialVersionUID = 1

    Project project
    Team team

    Date dateCreated
    Date lastUpdated

    String uuid = UUID.randomUUID().toString()
    static constraints = {
        project(nullable: true)
        team(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        uuid(nullable: true)
    }
}
