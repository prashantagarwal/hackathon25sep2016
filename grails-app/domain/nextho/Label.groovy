package nextho


class Label {
    private static final long serialVersionUID = 1

    String name
    User createdBy
    String uuid=UUID.randomUUID().toString()
    static constraints = {
        name(nullable: true)
        createdBy(nullable: true)
        uuid(nullable: true)

    }
}
