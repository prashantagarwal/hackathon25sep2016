package nextho

/**
 * Created with IntelliJ IDEA.
 * User: gdboling
 * Date: 8/23/13
 * Time: 3:37 PM
 */
class BootstrapService {

    def grailsApplication
    def roleBootstrapService
    def userBootstrapService
    def randomUserBootstrapService
    def teamBootstrapService


    def bootstrapUser() {

//        roleBootstrapService.bootstrap()
//        userBootstrapService.bootstrap()
//        randomUserBootstrapService.bootstrap()
//         teamBootstrapService.bootstrap()
    }

    void createIssue(Project project) {
        List<Issue> issues = []
        if (Issue.count == 0) {
            (1..4).each {

                int id = it
                User createdBy = User.findById(id)
                User assignedTo = User.findById(++id)

                Issue issue = new Issue()
                issue.project = project
                issue.title = "This is some serious Issue No. # ${project.id} ${it}"
                issue.description = "Description for ${issue.title}"
                issue.createdBy = createdBy
                issue.user = assignedTo
                Label label = Label.findById(it)
                label ?: issue.labels.add(label)
                issue.save(flush: true)
                issues.add(issue)
                println "*********** ISSUE CREATED : ${issue}"

            }
        }
        issues
    }

    void createLabels() {

        if (Label.count == 0) {
            List<String> labels = ["", "HIGH", "NORMAL", "LOW", "TODAY", "BUG"]
            (1..5).each {
                Label label = new Label()
                User user = User.findById(it)
                println "USER ${user}"

                label.createdBy = user
                label.name = labels[it]
                label.save(flush: true)
                println "********************** Created Label ${label}"
            }
        }

    }


    void createProjects() {

        if (Project.count == 0) {
            List<String> projectNames = ["P2PForce", "Fin360", "Voucherkart", "BakeryBite", "Zeetain", "DocReg", "GenRocket", "Arrow", "RoboLender"]

            8.times { int id ->
                Project project = new Project()
                project.name = projectNames[id]
                id++
                project.createdBy = User.findById(id as Long)



                project.save(flush: true)
                List<Issue> issues = createIssue(project)
                issues.each {
                    project.addToIssue(it)
                }
                project.save(flush: true)
                println "*************** Project Created : ${project}"
                createProjectUser(project)

            }

        }

    }

    void createProjectUser(Project project) {
        int projId = project.id as int
        int max = projId + 3

        (projId..max).each {
            User user = User.findById(it)
            ProjectUser projectUser = new ProjectUser()
            projectUser.user = user
            projectUser.project = project
            projectUser.access = Enums.Access.READ_WRITE
            projectUser.save(flush: true)
            println "****************** CREATED PROJECT USER ${projectUser.id}"
        }
    }

//    void createMileStone(){
//
//        Milestone milestone=new Milestone()
//
//    }

    void createComments() {

        if(Comment.count==0){
            1..30.each {
                Comment comment = new Comment()
                comment.comment = "This is Comment #${it}"
                User user = User.findById(it as Long)
                comment.createdBy = user
                comment.save(flush: true)
            }
        }

    }

    void createMileStones(){

        if(Milestone.count==0) {
            List<String> milestoneNames=["SEPT","OCT","NOV","DEC"]

        milestoneNames.each {ind,val->
            Milestone milestone = new Milestone()
            milestone.name=milestoneNames[0]
            ind=ind++
            milestone.createdby=User.findById(ind as Long)
            milestone.startDate=new Date()
            milestone.endDate=new Date()+20
            milestone.project=Project.findById(ind)
            milestone.save(flush: true)
            println "************** MILESTONE CREATED ${milestone}"
        }

        }

    }

    void createMileStoneTickets(){
        if(MileStoneTicket.count==0){
            1..10.each {
                Issue issue=Issue.findById(it)
                Milestone milestone=Milestone.findById(1)
                MileStoneTicket mileStoneTicket= new MileStoneTicket()
                mileStoneTicket.milestone=milestone
                mileStoneTicket.issue=issue
                mileStoneTicket.save(flush: true)
                println "*************** MILESTONE TIVKET CREATED ${mileStoneTicket}"

            }


        }
    }

}
