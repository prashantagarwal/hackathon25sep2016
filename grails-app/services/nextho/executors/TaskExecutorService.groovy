package nextho.executors

import nextho.Enums
import nextho.NotificationQueue

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor

import grails.transaction.Transactional

import java.util.concurrent.TimeUnit

@Transactional
class TaskExecutorService {


    def executeTask() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool()
        for (int i = 0; i <= 5; i++) {
            MyTask task = new MyTask("Task " + i)
            println("A new task has been added : " + task.getName())
            executor.execute(task)
        }
        executor.shutdown()

    }

    def scheduledExecutor() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Task task1 = new Task("Demo Task 1");

        System.out.println("The time is : " + new Date());

        ScheduledFuture<?> result = executor.scheduleWithFixedDelay(task1, 0, (60 * 5), TimeUnit.SECONDS);

        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        executor.shutdown();
    }

    def fetchNotificationQueuesAndSendMail() {

        List<Long> notificationQueues = NotificationQueue.createCriteria().list {
            'eq'('status', Enums.NotificationQueueStatus.QUEUE)
            projections {
                property('id')
            }
        }

        println "################ FOLLOWING QUEUES ARE PENDING ################"
        println notificationQueues

        notificationQueues.each { Long id ->

            String idString=id.toString()
            rabbitSend "mailQueue", "${idString}"
        }

    }


}
