package nextho

import nextho.User
import nextho.UserRole
import org.springframework.transaction.annotation.Transactional

/**
 * Created with IntelliJ IDEA.
 * User: gdboling
 * Date: 8/23/13
 * Time: 3:39 PM
 */
class UserBootstrapService {
    static transactional = false



    @Transactional
    def bootstrap() {
        saveIfNotExists(
                new User(
//                        externalId: externalIdService.generateUserExternalId(),
                        username: 'regadmin',
                        password: 'password',
                        firstName: 'Registry',
                        lastName: 'Admin',
                        emailAddress: 'regadmin@docreg.com',
                        enabled: true),
                ["ROLE_USER"]
        )

    }

    @Transactional
    def saveIfNotExists(User user, List<String> roles) {

        if(!Role.findByAuthority('ROLE_USER')) {
            Role role = Role.newInstance()
            role.authority='ROLE_USER'
            role.save()
        }

        def existingUser = User.findByUsername(user.username)

        if (!existingUser) {
            user.save()
            existingUser = user
        }

        roles.each { String role ->
            def authority = Role.findByAuthority('ROLE_USER')
            def existingUserRole = UserRole.get(existingUser.id, authority.id)
            if (!existingUserRole) {
                UserRole.create(existingUser, authority)
            }
        }
    }
}
