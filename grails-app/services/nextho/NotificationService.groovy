package nextho

import DTO.MailDTO
import grails.transaction.Transactional

@Transactional
class NotificationService {

    def groovyPageRenderer

    def projectCreatedNotification(User user){
        NotificationQueue notificationQueue=new NotificationQueue()
        notificationQueue.emailTo=user.username
        notificationQueue.subject="New Project"
        notificationQueue.content=projectCreatedContent(user)
        notificationQueue.isRead=Boolean.FALSE
        notificationQueue.status=Enums.NotificationQueueStatus.QUEUE
        notificationQueue.save(flush: true)

    }

    def ticketCreatedNotification(Issue issue){

        NotificationQueue notificationQueue=new NotificationQueue()
        notificationQueue.emailTo=issue.user.username
        notificationQueue.subject="Ticket Assigned"
        notificationQueue.content=ticketAssignedContent(issue)
        notificationQueue.isRead=Boolean.FALSE
        notificationQueue.status=Enums.NotificationQueueStatus.QUEUE
        notificationQueue.save(flush: true)


    }

    def userAssignedToProjectNotification(Project project,User user){
        NotificationQueue notificationQueue=new NotificationQueue()
        notificationQueue.emailTo=user.username
        notificationQueue.subject="Project Assigned"
        notificationQueue.content=projectAssignedContent(project,user)
        notificationQueue.isRead=Boolean.FALSE
        notificationQueue.status=Enums.NotificationQueueStatus.QUEUE
        notificationQueue.save(flush: true)

    }

    def labelAssignedToProjectNotification(){

    }

    def MileStoneCreatedNotification(){

    }


    String projectCreatedContent(User user,String from=null){

        String body="A New Project is Created"
        MailDTO mailDTO=new MailDTO(to:user.username,from:from?:'',body:body,teamName: "",role: "")
        groovyPageRenderer.render(template: '/public/viewMail',model: [mailDTO:mailDTO])

    }

    String ticketAssignedContent(Issue issue){
        String body="A New Ticket is Assigned"
        MailDTO mailDTO=new MailDTO(to:issue.user,from:issue.createdBy.username?:'',body:body,teamName: "",role: "")
        groovyPageRenderer.render(template: '/public/viewMail',model: [mailDTO:mailDTO])
    }

    String projectAssignedContent(Project project,User user){
        String body="A new project is Assigned to you"
        MailDTO mailDTO=new MailDTO(to:user.username,from:project.createdBy.fullName?:'',body:body,teamName: "",role: "")
        groovyPageRenderer.render(template: '/public/viewMail',model: [mailDTO:mailDTO])
    }


}
