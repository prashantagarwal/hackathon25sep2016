package nextho.rabbit

import grails.transaction.Transactional
import nextho.Email
import nextho.Enums
import nextho.NotificationQueue

@Transactional
class RabbitService {

    static rabbitQueue = "mailQueue"

    void handleMessage(String notificationQueueId) {

        NotificationQueue notificationQueue = NotificationQueue.findById(notificationQueueId as Long)
        try {

            println " ************** HANDLE MESSAGE **************"

            if (notificationQueue) {
                String sendTo = notificationQueue.emailTo
                String apiResponse = Email.sendEmail(sendTo)
                println "************ API reposnse : ${apiResponse}"
                notificationQueue.status = Enums.NotificationQueueStatus.SENT
                notificationQueue.save(flush: true)
            }


        } catch (Exception e) {
            println "############## EXCEPTION WHILE SENDING MAIL : ${e}"
        }
        finally {
            if (notificationQueue && notificationQueue.status.equals(Enums.NotificationQueueStatus.QUEUE)) {
                notificationQueue.status = Enums.NotificationQueueStatus.FAILED
                notificationQueue.save(flush: true)
            }
        }

    }
}
