package nextho

import grails.transaction.Transactional

@Transactional
class RandomUserBootstrapService {

    def bootstrap() {


        if(User.list().size() <100)
        {

            String prePath=AppUtil.homePath
            File file = new File(prePath+'hackathon25sep2016/data/users.csv')
            println file.exists()

            int i=0;
            file.eachCsvLine {def tokens ->

                if (i == 0) {

                } else {

                    User user = new User()
                    user.username = tokens[0]
                    user.firstName = tokens[1]
                    user.lastName = tokens[2]
                    user.emailAddress = tokens[3]
                    user.password = 'password'
                    user.save(flush:true)
                }
                i++

            }}





    if(!Role.findByAuthority(Roles.ROLE_ADMIN.toString()))
    {
       Role role=   Role.newInstance()
               role.authority=Roles.ROLE_ADMIN
        role.save()
    }

        if(!Role.findByAuthority(Roles.ROLE_DEV.toString()))
        {
            Role role=   Role.newInstance()
            role.authority=Roles.ROLE_DEV.toString()
            role.save()
        }



        if(UserRole.list().size()<10)
    {
    List firstTenUsers=User.list(max:10)
    List remainingUsers=User.list() - firstTenUsers
    firstTenUsers.each{
        UserRole.create(it,Role.findByAuthority(Roles.ROLE_ADMIN.toString()),true)

    }
    remainingUsers.each{

        UserRole.create(it,Role.findByAuthority(Roles.ROLE_DEV.toString()),true)
    }

    }


    }


}
