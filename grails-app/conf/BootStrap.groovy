

class BootStrap {

    def bootstrapService
    def grailsApplication
    def teamBootstrapService
    def userBootstrapService
    def randomUserBootstrapService
    def init = { servletContext ->

        bootstrapService.bootstrapUser()
        userBootstrapService.bootstrap()
        randomUserBootstrapService.bootstrap()
        teamBootstrapService.bootstrap()

        bootstrapService.createLabels()
        bootstrapService.createProjects()
        bootstrapService.createComments()
        bootstrapService.createMileStones()
        bootstrapService.createMileStoneTickets()

    }

    def destroy = {
    }
}
