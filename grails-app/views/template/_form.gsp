<div class="col-md-6 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><strong> Form</strong></h2>
        </div>
        <div class="panel-body">
            <form action="" method="post" class="form-horizontal ">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="hf-email">Email</label>
                    <div class="col-md-9">
                        <input type="email" id="hf-email" name="hf-email" class="form-control" placeholder="Enter Email..">
                        <span class="help-block">Please enter your email</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="hf-password">Password</label>
                    <div class="col-md-9">
                        <input type="password" id="hf-password" name="hf-password" class="form-control" placeholder="Enter Password..">
                        <span class="help-block">Please enter your password</span>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
            <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
        </div>
    </div>

</div>