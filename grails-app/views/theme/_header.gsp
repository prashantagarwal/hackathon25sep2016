<div class="navbar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">a</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html#"><img src="assets/img/logo.png"> cleverAdmin</a>
        </div>

        <ul class="nav navbar-nav navbar-right">

            <li><a href="logout">Logout</a></li>
        </ul>
    </div>
</div>