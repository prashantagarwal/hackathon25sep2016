<div class="sidebar col-md-2 col-sm-1 ">

    <div class="sidebar-collapse collapse">

        <div class="nav-sidebar title"><span>Main Menu</span></div>

        <ul class="nav nav-sidebar">

            <li><g:link controller="user" action="list"><i class="fa fa-bar-chart-o"></i><span
                    class="hidden-sm text">User</span></g:link></li>
            <li><g:link controller="team" action="index"><i class="fa fa-bar-chart-o"></i><span
                    class="hidden-sm text">Team</span></g:link></li>

            <li><g:link controller="project" action="index"><i class="fa fa-bar-chart-o"></i><span
                    class="hidden-sm text">Projects</span></g:link></li>

            <li><g:link controller="label" action="index"><i class="fa fa-bar-chart-o"></i><span
                    class="hidden-sm text">Labels</span></g:link></li>
            <li><g:link controller="issues" action="index"><i class="fa fa-bar-chart-o"></i><span
                    class="hidden-sm text">Issue</span></g:link></li>
            <li><g:link controller="mixPanel" action="index"><i class="fa fa-bar-chart-o"></i><span
                    class="hidden-sm text">Track</span></g:link></li>
        </ul>

    </div>

</div>