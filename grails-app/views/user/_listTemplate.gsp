<%@ page import="nextho.User" %>

<div class="col-lg-10">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-list"></i>User Details</h2>
        </div>
        <div class="panel-body">
            <table class="table bootstrap-datatable datatable small-font">
                <thead>
                <tr>
                    <th><util:remoteSortableColumn total="${User.count()}"  property="id" title="${message(code: 'book.id.label', default: 'Id')}" update="filteredList" action="filter"/></th>
                    <th><util:remoteSortableColumn property="username" total="${User.count()}" title="${message(code: 'book.author.label', default: 'UserName')}" update="filteredList" action="filter"/></th>
                    <th><util:remoteSortableColumn property="firstName" total="${User.count()}" title="${message(code: 'book.name.label', default: 'First Name')}" update="filteredList" action="filter"/></th>
                    <th><util:remoteSortableColumn property="lastName" total="${User.count()}" title="${message(code: 'book.price.label', default: 'Last Name')}" update="filteredList" action="filter"/></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${users}" status="i" var="bookInstance">
                    <tr>
                        <td></td>  <td><span class="label label-success"><g:link action="show" id="${bookInstance.id}">${fieldValue(bean: bookInstance, field: "id")}</g:link></span></td>
                        <td></td>  <td>${fieldValue(bean: bookInstance, field: "username")}</td>
                        <td></td>  <td>${fieldValue(bean: bookInstance, field: "firstName")}</td>
                        <td></td>  <td>${fieldValue(bean: bookInstance, field: "lastName")}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div>
    <div class="list">
        %{--<table>--}%
        %{--<thead>--}%
        %{--<tr>--}%

        %{--<util:remoteSortableColumn total="${User.count()}"  property="id" title="${message(code: 'book.id.label', default: 'Id')}" update="filteredList" action="filter"/>--}%

        %{--<util:remoteSortableColumn property="username" total="${User.count()}" title="${message(code: 'book.author.label', default: 'UserName')}" update="filteredList" action="filter"/>--}%

        %{--<util:remoteSortableColumn property="firstName" total="${User.count()}" title="${message(code: 'book.name.label', default: 'First Name')}" update="filteredList" action="filter"/>--}%

        %{--<util:remoteSortableColumn property="lastName" total="${User.count()}" title="${message(code: 'book.price.label', default: 'Last Name')}" update="filteredList" action="filter"/>--}%

        %{--</tr>--}%
        %{--</thead>--}%
        %{--<tbody>--}%
        %{--<g:each in="${users}" status="i" var="bookInstance">--}%
        %{--<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">--}%

        %{--<td><g:link action="show" id="${bookInstance.id}">${fieldValue(bean: bookInstance, field: "id")}</g:link></td>--}%

        %{--<td>${fieldValue(bean: bookInstance, field: "username")}</td>--}%

        %{--<td>${fieldValue(bean: bookInstance, field: "firstName")}</td>--}%

        %{--<td>${fieldValue(bean: bookInstance, field: "lastName")}</td>--}%

        %{--</tr>--}%
        %{--</g:each>--}%
        %{--</tbody>--}%
        %{--</table>--}%
    </div>
    <div class="paginateButtons">
        <util:remotePageScroll total="${User.count()}" update="filteredList"     action="filter" pageSizes="[5: '5 on Page',10:'10 on Page',15:'15 on Page']" max="5" />
    </div>
</div>