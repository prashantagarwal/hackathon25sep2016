<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="nextho"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>


<body>
<div class="row">
    <div class="col-md-2"></div>

    <div class="col-md-9">

        <div class="main-box-body clearfix">
            <div class="panel-group accordion" id="accordion">

                <div class="panel panel-default">

                    <strong>Issue</strong>


                    <div id="collapseAddress" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <button type="button" class="md-trigger btn btn-primary mrg-b-lg"
                                    style="margin-bottom: 0;float: right"
                                    onclick="resetIssueDetailsFromModal()"
                                    data-toggle="modal"
                                    data-target="#modal-issue">Add Issue</button>


                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="main-box-body clearfix">
                                            <div class="table-responsive clearfix">

                                                <div id="employmentCategoryTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>

                                                            <th>#</th>
                                                            <th><span>Name</span></th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <g:each in="${issueList}" status="index"
                                                                var="issue">

                                                            <tr><td>
                                                                ${index + 1}
                                                            </td>
                                                                <td id="issue_name_${issue.uuid}">${issue.name ?: '-'}</td>

                                                                <td>
                                                                    <button type="button"
                                                                            class=" btn btn-sm btn-primary"
                                                                            onclick='updateIssueDetail("${issue.uuid}")'
                                                                            data-toggle="modal"
                                                                            data-target="#modal-issue">Edit</button>


                                                                    <g:link class=" btn btn-sm btn-danger"
                                                                            controller="issues"
                                                                            action="deleteIssue"
                                                                            name="issueDeleteLink"
                                                                            params="[id: issue.uuid]">Delete</g:link>

                                                                </td>

                                                            </tr>

                                                        </g:each>

                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-1"></div>

</div>
<g:render template="/issue/modalForIssue"/>


<script>
    function updateIssueDetail(uuid) {


        var issueName = $('#issue_name_' + uuid).html();


        $('#issueName').val(issueName);


        $("#url").val('updateIssue');
        $("#uuid").val('' + uuid);


    }


    function resetIssueDetailsFromModal() {

        $("#issueName").val('');
        $("#url").val('createIssue');


    }

    $(function () {
        $('a[name="issueDeleteLink"]').bind('click', function () {

            if (confirm('Are you Sure?')) {
            }
            else {
                $(this).attr('href', "");
            }
        })
    })
</script>

</body>
</html>