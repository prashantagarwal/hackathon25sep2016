<div class="modal fade" id="modal-issue" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Issue</h4>
            </div>

        <div class="modal-body">
            <g:form id="issueForm" controller="issues" action="createUpdateIssue">
                <div class="form-group">
                    <label for="issueName">Issue Name</label>
                    <input type="text" class="form-control" id="issueName" name="name"
                           placeholder="Name">
                </div>



                </div>
                <input type="hidden" name="url" id="url"/>
                <input type="hidden" name="uuid" id="uuid"/>


                <div class="modal-footer">
                    <button type="submit" id="" class="btn btn-primary">Done</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </g:form>

        </div>

    </div>
</div>




<script>
    function resetUpdateValue() {
        $('#modalwindow').modal('hide');
    }
</script>


