<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Issue Detail : ${issue?.name ?: " "}</title>
    <meta name="layout" content="nextho"/>
</head>

<body>

<div class="container-fluid content">

    <g:render template="/common/statsBlocks"/>

    <div class="row">
        <div class="col-md-8 col-sm-8 main col-md-offset-2 ">

            <div class="row">
                <div class="col-sm-12">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">${issue?.title ?: ''}</h3>
                        </div>

                        <div class="panel-body">
                            <p>
                                %{--${issue.}--}%
                            </p>
                        </div>
                    </div>

                </div><!--/col-->

            </div><!--/row-->

        </div>

    </div><!--/row-->

</div><!--/container-->

%{--</div>--}%
</body>
</html>