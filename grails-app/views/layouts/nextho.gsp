<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="CleverAdmin - Bootstrap Admin Template">
		<meta name="author" content="Łukasz Holeczek">
		<meta name="keyword" content="CleverAdmin, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="assets/ico/favicon.png">

	    <title>cleverAdmin - Bootstrap Admin Template</title>

	    <!-- Bootstrap core CSS -->
	    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
<asset:stylesheet href="css/bootstrap.min.css"/>

<!-- page css files -->
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<link href="assets/css/jquery-ui.min.css" rel="stylesheet">
<link href="assets/css/fullcalendar.css" rel="stylesheet">
<link href="assets/css/morris.css" rel="stylesheet">
<link href="assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<link href="assets/css/climacons-font.css" rel="stylesheet">



<asset:stylesheet href="css/font-awesome.min.css"/>
<asset:stylesheet href="css/jquery-ui.min.css"/>
<asset:stylesheet href="css/fullcalendar.css"/>
<asset:stylesheet href="css/morris.css"/>
<asset:stylesheet href="css/jquery-jvectormap-1.2.2.css"/>
<asset:stylesheet href="css/climacons-font.css"/>




<!-- Custom styles for this template -->
<link href="assets/css/style.min.css" rel="stylesheet">

<asset:stylesheet href="css/style.min.css"/>



<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<g:layoutHead/>
<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
        0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("3253cc0c06750f432a4d7902994875fa");</script><!-- end Mixpanel -->
</head>

<body>
<!-- start: Header -->
<g:render template="/theme/header"/>
<!-- end: Header -->

<div class="container-fluid content">
    <div class="row" style="
    min-height: 560px;
    ">

        <!-- start: Main Menu -->
        <g:render template="/theme/leftNav"/>
        <!-- end: Main Menu -->

        <g:layoutBody/>
        <!-- start: Content -->
        %{--<g:render template="/theme/body"/>--}%
        <!-- end: Content -->

    </div><!--/row-->

</div><!--/container-->




<!-- /.modal -->

<div class="clearfix"></div>

<g:render template="/theme/footer"/>

<!-- start: JavaScript-->
<!--[if !IE]>-->

<script src="assets/js/jquery-3.1.0.min.js"></script>

<!--<![endif]-->

<!--[if IE]>

		<script src="assets/js/jquery-1.11.0.min.js"></script>


	<![endif]-->

<!--[if !IE]>-->

%{--<script type="text/javascript">--}%
%{--window.jQuery || document.write("<script src='assets/js/jquery-3.1.0.min.js'>" + "<" + "/script>");--}%
%{--</script>--}%

<!--<![endif]-->

<!--[if IE]>

		<script type="text/javascript">
	 	window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
		</script>

	<![endif]-->
%{--<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>--}%
%{--<script src="assets/js/bootstrap.min.js"></script>--}%

<asset:javascript src="js/jquery-migrate-1.2.1.min.js"/>
<asset:javascript src="js/bootstrap.min.js"/>


<!-- page scripts -->
%{--<script src="assets/js/jquery-ui.min.js"></script>--}%
%{--<script src="assets/js/jquery.ui.touch-punch.min.js"></script>--}%
%{--<script src="assets/js/jquery.sparkline.min.js"></script>--}%
%{--<script src="assets/js/fullcalendar.min.js"></script>--}%
%{--<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="assets/js/excanvas.min.js"></script><![endif]-->--}%
%{--<script src="assets/js/jquery.flot.min.js"></script>--}%
%{--<script src="assets/js/jquery.flot.pie.min.js"></script>--}%
%{--<script src="assets/js/jquery.flot.stack.min.js"></script>--}%
%{--<script src="assets/js/jquery.flot.resize.min.js"></script>--}%
%{--<script src="assets/js/jquery.flot.time.min.js"></script>--}%
%{--<script src="assets/js/jquery.flot.spline.min.js"></script>--}%
%{--<script src="assets/js/jquery.autosize.min.js"></script>--}%
%{--<script src="assets/js/jquery.placeholder.min.js"></script>--}%
%{--<script src="assets/js/moment.min.js"></script>--}%
%{--<script src="assets/js/daterangepicker.min.js"></script>--}%
%{--<script src="assets/js/jquery.easy-pie-chart.min.js"></script>--}%
%{--<script src="assets/js/jquery.dataTables.min.js"></script>--}%
%{--<script src="assets/js/dataTables.bootstrap.min.js"></script>--}%
%{--<script src="assets/js/raphael.min.js"></script>--}%
%{--<script src="assets/js/morris.min.js"></script>--}%
%{--<script src="assets/js/jquery-jvectormap-1.2.2.min.js"></script>--}%
%{--<script src="assets/js/uncompressed/jquery-jvectormap-world-mill-en.js"></script>--}%
%{--<script src="assets/js/uncompressed/gdp-data.js"></script>--}%
%{--<script src="assets/js/gauge.min.js"></script>--}%


<asset:javascript src="js/jquery-ui.min.js"/>
<asset:javascript src="js/jquery.ui.touch-punch.min.js"/>
<asset:javascript src="js/jquery.sparkline.min.js"/>
<asset:javascript src="js/fullcalendar.min.js"/>

<asset:javascript src="js/js/excanvas.min.js"/>

<asset:javascript src="js/jquery.flot.min.js"/>
<asset:javascript src="js/jquery.flot.pie.min.js"/>
<asset:javascript src="jquery.flot.stack.min.js"/>
<asset:javascript src="js/jquery.flot.resize.min.js"/>
<asset:javascript src="js/jquery.flot.time.min.js"/>
<asset:javascript src="js/jquery.flot.spline.min.js"/>
<asset:javascript src="js/jquery.autosize.min.js"/>
<asset:javascript src="js/jquery.placeholder.min.js"/>
<asset:javascript src="js/moment.min.js"/>
<asset:javascript src="js/daterangepicker.min.js"/>
<asset:javascript src="js/jquery.easy-pie-chart.min.js"/>
<asset:javascript src="js/jquery.dataTables.min.js"/>
<asset:javascript src="js/dataTables.bootstrap.min.js"/>
<asset:javascript src="js/raphael.min.js"/>
<asset:javascript src="js/morris.min.js"/>
<asset:javascript src="js/jquery-jvectormap-1.2.2.min.js"/>
<asset:javascript src="js/uncompressed/jquery-jvectormap-world-mill-en.js"/>
<asset:javascript src="js/uncompressed/gdp-data.js"/>
<asset:javascript src="js/gauge.min.js"/>




<!-- theme scripts -->
%{--<script src="assets/js/custom.min.js"></script>--}%
%{--<script src="assets/js/core.min.js"></script>--}%

<asset:javascript src="js/custom.min.js"/>
<asset:javascript src="js/core.min.js"/>

<!-- inline scripts related to this page -->
%{--<script src="assets/js/pages/index.js"></script>--}%
<asset:javascript src="js/pages/index.js"/>


<!-- end: JavaScript-->
<script>
    mixpanel.identify("${user?.id}");
    mixpanel.people.set({
        "$first_name": "${user?.firstName}",
        "$last_name": "${user?.lastName}",
        "$email": "${user?.emailAddress}"
    });</script>
</body>
</html>