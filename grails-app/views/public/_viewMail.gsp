<%--
  Created by IntelliJ IDEA.
  User: chetan
  Date: 24/9/16
  Time: 4:27 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div marginwidth="0" marginheight="0" style="min-height:100%;margin:0;padding:0;width:100%;background-color:#f2f2f2">
    <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#f2f2f2">
            <tbody><tr>
                <td align="center" valign="top" style="height:100%;margin:0;padding:0;width:100%;border-top:0">

                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                        <tbody><tr>
                            <td align="center" valign="top">


                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="640" style="border-collapse:collapse">
                                    <tbody><tr>
                                        <td align="center" valign="top" width="60" style="background-color:#6dc6dd">
                                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                <tbody><tr>
                                                    <td align="center" valign="top" style="padding-top:18px;padding-right:4px;padding-bottom:18px;padding-left:4px">
                                                        <img src="https://ci4.googleusercontent.com/proxy/BHvkhT7tEAxM_FucQ3h5sgoghKLyFQGPx809B8DRWiUEDpWEEmNwcx5TAKdGHXP1eIK3xG1XUk5r311T7vqXo-p6xe5db9aLJG-BxV9FYA5svUo9c_OcnDTaOQkHiqrb8PJ7kFw=s0-d-e1-ft#http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/icon_envelope.png" height="40" width="40" style="border:0;display:block;line-height:100%;outline:none;text-decoration:none;min-height:auto;vertical-align:bottom" class="CToWUd">
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </td>
                                        <td align="center" valign="top" style="padding-bottom:9px;padding-left:9px">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                <tbody><tr>
                                                    <td align="center" valign="top" style="padding-top:9px">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#f2f2f2;border-top:0;border-bottom:0">
                                                            <tbody><tr>
                                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top" style="padding:9px">
                                                                            <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%;border-collapse:collapse">
                                                                                <tbody><tr>
                                                                                    <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center">


                                                                                        <img align="center" alt="" src="http://nexthoughts.com/img/logo.png" width="233" style="max-width:233px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;min-height:auto;outline:none;text-decoration:none" class="CToWUd">


                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                            </tbody></table>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top">

                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#f2f2f2;border-top:0;border-bottom:0">
                                                            <tbody><tr>
                                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top" style="padding-top:9px">



                                                                </table>



                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
                                                        <tbody>
                                                        <tr>
                                                            <td valign="top" style="padding-top:9px">



                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                                                                    <tbody><tr>

                                                                        <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                                                                            <strong>Dear ${mailDTO.to}</strong>,<br><br> ${mailDTO.body}.<br><br>Sincerely,<br>${mailDTO.from}<br><em>${mailDTO.role}, ${mailDTO.teamName}</em><br><br>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody></table>



                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                                </tbody></table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">

                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#f2f2f2;border-top:1px solid #e5e5e5;border-bottom:0">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-bottom:9px"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse">
                                                        <tbody>
                                                        <tr>
                                                            <td valign="top" style="padding-top:9px">







                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                                </tbody></table>

                                        </td>
                                    </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        </tbody></table>
                </td>
            </tr>
            </tbody></table>

    </td>
    </tr>
    </tbody></table>
    </center><div class="yj6qo"></div><div class="adL">
</div></div>
</body>
</html>