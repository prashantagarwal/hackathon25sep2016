<div class="row">



    <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
        <div class="smallstat">
            <div class="boxchart-overlay blue">
                <div class="boxchart">
                    <span class="fa fa-user"></span>
                    %{--<canvas width="64" height="30" style="display: inline-block; width: 64px; height: 30px; vertical-align: top;"></canvas>--}%
                </div>
            </div>
            <span class="value">4 589</span>
            <span class="title">Clients</span>

        </div>
    </div><!--/col-->

    <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
        <div class="smallstat">
            <div class="linechart-overlay red">
                <div class="linechart">
                    <span class="fa fa-user"></span>
                    %{--<canvas width="65" height="30" style="display: inline-block; width: 65px; height: 30px; vertical-align: top;"></canvas>--}%
                </div>
            </div>
            <span class="value">789</span>
            <span class="title">Deals</span>
        </div>
    </div><!--/col-->

    <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
        <div class="smallstat">
            <i class="fa fa-usd green"></i>
            <span class="value">$1 999,99</span>
            <span class="title">Income</span>
        </div>
    </div><!--/col-->

    <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
        <div class="smallstat">
            <div class="piechart-overlay blue">
                <div class="piechart easyPieChart" data-percent="55" style="width: 40px; height: 40px; line-height: 40px;"><span>54</span>%<canvas width="40" height="40"></canvas></div>
            </div>
            <span class="value">$19 999,99</span>
            <span class="title">Account</span>
        </div>
    </div><!--/col-->

</div>