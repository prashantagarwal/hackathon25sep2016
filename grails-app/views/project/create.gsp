<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="nextho">

    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>

<body>
<div id="filteredList">
    <br>
    <br>

    <%@ page import="nextho.User" %>

    <div class="col-lg-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1><i class="fa fa-list"></i>Project</h1>
            </div>
            <g:form controller="project" action="save">

                <div class="panel-body">
                    <table class="table bootstrap-datatable datatable small-font">
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label for="name" class="col-lg-4 control-label">Name</label>

                                    <div class="col-lg-6">
                                        <input type="text" name="name" id="name" class="form-control">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label for="team" class="col-lg-4 control-label">Team</label>

                                    <div class="col-lg-6">

                                        <g:select noSelection="['': 'Select']" id="team" onchange="addTeam()"
                                                  class="form-control" from="${teamList}"
                                                  optionValue="name"
                                                  name="team"
                                                  tabindex="-1"
                                                  optionKey="uuid"/>
                                    </div>

                                    %{--<div class="col-lg-2">--}%
                                    %{--<g:link  class="md-trigger btn btn-primary mrg-b-lg"--}%
                                    %{--style="margin-bottom: 0;float: right">Create Team</g:link>--}%
                                    %{--</div>--}%
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div class="form-group">
                                    <label for="milestone" class="col-lg-4 control-label">Milestone</label>

                                    <div class="col-lg-6">
                                        <input type="text" name="milestone" id="milestone" class="form-control">
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <g:submitButton name="create" class="btn btn-large btn-primary"
                                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </g:form>
        </div>
    </div>

    <div>
        <div class="list">
            %{--<table>--}%
            %{--<thead>--}%
            %{--<tr>--}%

            %{--<util:remoteSortableColumn total="${User.count()}"  property="id" title="${message(code: 'book.id.label', default: 'Id')}" update="filteredList" action="filter"/>--}%

            %{--<util:remoteSortableColumn property="username" total="${User.count()}" title="${message(code: 'book.author.label', default: 'UserName')}" update="filteredList" action="filter"/>--}%

            %{--<util:remoteSortableColumn property="firstName" total="${User.count()}" title="${message(code: 'book.name.label', default: 'First Name')}" update="filteredList" action="filter"/>--}%

            %{--<util:remoteSortableColumn property="lastName" total="${User.count()}" title="${message(code: 'book.price.label', default: 'Last Name')}" update="filteredList" action="filter"/>--}%

            %{--</tr>--}%
            %{--</thead>--}%
            %{--<tbody>--}%
            %{--<g:each in="${users}" status="i" var="bookInstance">--}%
            %{--<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">--}%

            %{--<td><g:link action="show" id="${bookInstance.id}">${fieldValue(bean: bookInstance, field: "id")}</g:link></td>--}%

            %{--<td>${fieldValue(bean: bookInstance, field: "username")}</td>--}%

            %{--<td>${fieldValue(bean: bookInstance, field: "firstName")}</td>--}%

            %{--<td>${fieldValue(bean: bookInstance, field: "lastName")}</td>--}%

            %{--</tr>--}%
            %{--</g:each>--}%
            %{--</tbody>--}%
            %{--</table>--}%
        </div>

    </div>
</div>
<script>
    function addTeam() {
        %{--var loanApplicationId = $('#loanApplicationId').val();--}%
        %{--var tagId = $('#tag').val();--}%
        %{--$.ajax({--}%
        %{--url: "${createLink(controller: 'loanApplication', action: 'addTag')}",--}%
        %{--data: {'loanApplicationId': loanApplicationId, tagId: tagId},--}%
        %{--success: function (data) {--}%
        %{--if (data.status == 'Tag Already Exist') {--}%
        %{--$('#loanOverviewTable').html(data.template);--}%
        %{--toastr.error("Tag Already Exist");--}%
        %{--}--}%
        %{--else if (data.status == 'Successfull') {--}%
        %{--$('#loanOverviewTable').html(data.template);--}%
        %{--toastr.success("Tag Added Succesfully");--}%

        %{--}--}%
        %{--},--}%
        %{--error: function () {--}%
        %{--toastr.error("Internal Error");--}%

        %{--}--}%
        %{--})--}%

    }
    function addIssue() {

    }
    function addLabel() {

    }
</script>
</body>
</html>