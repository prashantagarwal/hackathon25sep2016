<div class="modal fade" id="modal-issue" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Issue</h4>
            </div>

        <div class="modal-body">
            <g:form id="issueForm" controller="project" action="createIssue">
                <div class="form-group">
                    <label for="title">Issue Name</label>
                    <input type="text" class="form-control" id="title" name="title"
                           placeholder="Name">
                </div>

                <div class="form-group">
                    <label for="description">Issue Name</label>
                    <input type="text" class="form-control" id="description" name="description"
                           placeholder="Description">
                </div>

                <div class="form-group">
                    <label for="user">User</label>
                    <g:select noSelection="['': 'Select User']" id="team" onchange="addTeam()"
                              class="form-control" from="${nextho.AppUtil.userList()}"
                              optionValue="username"
                              name="user"
                              tabindex="-1"
                              optionKey="uuid"/>
                </div>



                </div>
                <input type="hidden" name="url" id="url"/>
                <input type="hidden" name="uuid" id="uuid"/>
                <input type="hidden" name="projectUuid" id="${project.uuid}"/>


                <div class="modal-footer">
                    <button type="submit" id="" class="btn btn-primary">${project.uuid}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </g:form>

        </div>

    </div>
</div>




<script>
    function resetUpdateValue() {
        $('#modalwindow').modal('hide');
    }
</script>


