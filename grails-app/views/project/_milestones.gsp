<div class="form-group" id="milestoneList">
    <label for="milestoneId" class="col-lg-4 control-label">Milestone</label>
    <g:hiddenField name="projectId" id="projectId" value="${project?.uuid}"/>
    <div class="col-lg-6">

        <g:each in="${project?.milestones as List}" var="milestone">
            <button type="button" class="btn" id="${milestone.uuid}"
                    name="milestoneId" id="milestoneId"
                    style="background-color:#FF0000;color: #FFFFFF; ">
                ${milestone?.name}
                <span class="badge"
                      style="color:#000000;background-color:#FFFFFF">x</span>


            </button>
        </g:each>
    </div>
</div>