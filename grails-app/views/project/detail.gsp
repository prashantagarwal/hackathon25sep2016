<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="nextho">

    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>

<body>
<div id="filteredList">
    <br>
    <br>

    <%@ page import="nextho.User" %>

    <div class="col-lg-10">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1><i class="fa fa-list"></i>Project</h1>

        </div>

        <g:form controller="project" action="save">

            <div class="panel-body">
                <table class="table bootstrap-datatable datatable small-font">
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="name" class="col-lg-4 control-label">Name</label>

                                <div class="col-lg-6">
                                    <input type="text" name="name" id="name" class="form-control"
                                           value="${project?.name}">
                                </div>
                            </div>
                        </td>
                    </tr>
                    %{--<tr>--}%
                    %{--<td>--}%
                    %{--<div class="form-group">--}%
                    %{--<label for="team" class="col-lg-4 control-label">Team</label>--}%

                    %{--<div class="col-lg-6">--}%

                    %{--<g:each in="${project?.teams as List}" var="team">--}%
                    %{--<button type="button" class="btn" id="${team.uuid}"--}%
                    %{--name="team" id="team"--}%
                    %{--style="background-color:#800000;color: #FFFFFF; ">--}%
                    %{--${team?.name}--}%
                    %{--<span class="badge"--}%
                    %{--style="color:#000000;background-color:#FFFFFF">x</span>--}%

                    %{--</button>--}%
                    %{--</g:each>--}%
                    %{--</div>--}%

                    %{--<div class="col-lg-2">--}%
                    %{--<g:link  class="md-trigger btn btn-primary mrg-b-lg"--}%
                    %{--style="margin-bottom: 0;float: right">Create Team</g:link>--}%
                    %{--</div>--}%
                    %{--</div>--}%
                    %{--</td>--}%
                    %{--</tr>--}%


                    <tr>
                        <td>

                            <g:render template="/project/milestones" model="[project: project]"/>
                        </td>
                    </tr>

                    <tr>
                        <td>

            </div>

            <div class="col-lg-6">
                <button type="button" class="md-trigger btn btn-danger mrg-b-lg"
                        style="margin-bottom: 0;float: right"
                        onclick="resetIssueDetailsFromModal()"
                        data-toggle="modal"
                        data-target="#modal-issue">Add Issue</button>
            </div>
            </td>
        </tr>

            <tr>
                <td>
                    <g:submitButton name="create" class="btn btn-large btn-primary"
                                    value="Update"/>
                </td>
            </tr>
            </table>
        </div>
        </g:form>
    </div>
</div>

<div>
    <div class="list">
    </div>

</div>
</div>
<g:render template="/project/modalForIssue" model="[project: project]"/>

<script>
    function addTeam() {
    }
    function addIssue() {

    }
    function addLabel() {

    }


</script>
<script>
    $('[name="milestoneId"]').click(function () {
        var id = this.id;
        var projectId = $('#projectId').val();

        $.ajax({
            url: "${createLink(controller: 'project', action: 'removeMilestone')}",
            data: {'id': id, 'projectId': projectId},
            success: function (data) {
                $('#milestoneList').html(data.template);

            },
            error: function () {
                toastr.error("Internal Error");

            }
        })
    });
    $('[name="milestone"]').click(function () {
        alert(this.id);
        var id = this.id;
        var projectId = $('#projectId').val();

        $.ajax({
            url: "${createLink(controller: 'project', action: 'addMilestone')}",
            data: {'id': id, 'projectId': projectId},
            success: function (data) {
                $('#milestoneList').html(data.template);

            },
            error: function () {
                toastr.error("Internal Error");

            }
        })
    });

    function resetIssueDetailsFromModal() {

        $("#issueName").val('');
        $("#url").val('createIssue');


    }


</script>
</body>
</html>