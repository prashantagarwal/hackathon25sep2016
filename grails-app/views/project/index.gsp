<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="nextho"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>


<body>
<div class="row">
    <div class="col-md-2"></div>

    <div class="col-md-9">

        <div class="main-box-body clearfix">
            <div class="panel-group accordion" id="accordion">

                <div class="panel panel-default">

                    <h1><i class="fa fa-list"></i>Project</h1>


                    <div id="collapseAddress" class="panel-collapse collapse in">
                        <div class="panel-body">
                            %{--<button type="button" class="md-trigger btn btn-primary mrg-b-lg"--}%
                            %{--style="margin-bottom: 0;float: right"--}%
                            %{--onclick="resetProjectDetailsFromModal()"--}%
                            %{--data-toggle="modal"--}%
                            %{--data-target="#modal-project">Create Project</button>--}%

                            <g:link controller="project" action="create" class="md-trigger btn btn-primary mrg-b-lg"
                                    style="margin-bottom: 0;float: right">Create Project</g:link>


                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="main-box-body clearfix">
                                            <div class="table-responsive clearfix">

                                                <div id="employmentCategoryTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>

                                                            <th>#</th>
                                                            <th><span>Name</span></th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <g:each in="${projectList}" status="index"
                                                                var="project">

                                                            <tr><td>
                                                                ${index + 1}
                                                            </td>
                                                                <td id="project_name_${project.uuid}">${project.name ?: '-'}</td>

                                                                <td>
                                                                    <button type="button"
                                                                            class=" btn btn-sm btn-primary"
                                                                            onclick='updateProjectDetail("${project.uuid}")'
                                                                            data-toggle="modal"
                                                                            data-target="#modal-project">Edit</button>


                                                                    <g:link class=" btn btn-sm btn-danger"
                                                                            controller="project"
                                                                            action="deleteProject"
                                                                            name="projectDeleteLink"
                                                                            params="[id: project.uuid]">Delete</g:link>

                                                                </td>
                                                                <td>


                                                                    <g:link class=" btn btn-sm btn-success"
                                                                            controller="project"
                                                                            action="detail"
                                                                            params="[id: project.uuid]">Detail</g:link>

                                                                </td>

                                                            </tr>

                                                        </g:each>

                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-1"></div>

</div>
<g:render template="/project/modalForProject"/>


<script>
    function updateProjectDetail(uuid) {


        var projectName = $('#project_name_' + uuid).html();


        $('#projectName').val(projectName);


        $("#url").val('updateProject');
        $("#uuid").val('' + uuid);


    }


    function resetProjectDetailsFromModal() {

        $("#projectName").val('');
        $("#url").val('createProject');


    }

    $(function () {
        $('a[name="projectDeleteLink"]').bind('click', function () {

            if (confirm('Are you Sure?')) {
            }
            else {
                $(this).attr('href', "");
            }
        })
    })
</script>

</body>
</html>