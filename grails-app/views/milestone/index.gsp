<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="nextho"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>


<body>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-9">



        <div class="main-box-body clearfix">
            <div class="panel-group accordion" id="accordion">

                <div class="panel panel-default">

                   <strong>Milestone</strong>


                    <div id="collapseAddress" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <button type="button" class="md-trigger btn btn-primary mrg-b-lg"
                                    style="margin-bottom: 0;float: right"
                                    onclick="resetMilestoneDetailsFromModal()"
                                    data-toggle="modal"
                                    data-target="#modal-milestone">Add Milestone</button>


                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="main-box-body clearfix">
                                            <div class="table-responsive clearfix">

                                                <div id="employmentCategoryTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>

                                                            <th>#</th>
                                                            <th><span>Name</span></th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <g:each in="${milestoneList}" status="index"
                                                                var="milestone">

                                                            <tr><td>
                                                                ${index + 1}
                                                            </td>
                                                                <td id="milestone_name_${milestone.uuid}">${milestone.name ?: '-'}</td>

                                                                <td>
                                                                    <button type="button"
                                                                            class=" btn btn-sm btn-primary"
                                                                            onclick='updateMilestoneDetail("${milestone.uuid}")'
                                                                            data-toggle="modal"
                                                                            data-target="#modal-milestone">Edit</button>


                                                                    <g:link class=" btn btn-sm btn-danger"
                                                                            controller="milestone"
                                                                            action="deleteMilestone"
                                                                            name="milestoneDeleteLink"
                                                                            params="[id: milestone.uuid]">Delete</g:link>

                                                                </td>

                                                            </tr>

                                                        </g:each>

                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-1"></div>

</div>
<g:render template="/milestone/modalForMilestone"/>


<script>
    function updateMilestoneDetail(uuid) {


        var milestoneName = $('#milestone_name_' + uuid).html();


        $('#milestoneName').val(milestoneName);


        $("#url").val('updateMilestone');
        $("#uuid").val('' + uuid);


    }


    function resetMilestoneDetailsFromModal() {

        $("#milestoneName").val('');
        $("#url").val('createMilestone');


    }

    $(function () {
        $('a[name="milestoneDeleteLink"]').bind('click', function () {

            if (confirm('Are you Sure?')) {
            }
            else {
                $(this).attr('href', "");
            }
        })
    })
</script>

</body>
</html>