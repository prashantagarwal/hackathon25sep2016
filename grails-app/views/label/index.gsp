<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <title></title>
    <meta name="layout" content="nextho"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

</head>


<body>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-9">



        <div class="main-box-body clearfix">
            <div class="panel-group accordion" id="accordion">

                <div class="panel panel-default">

                   <strong>Label</strong>


                    <div id="collapseAddress" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <button type="button" class="md-trigger btn btn-primary mrg-b-lg"
                                    style="margin-bottom: 0;float: right"
                                    onclick="resetLabelDetailsFromModal()"
                                    data-toggle="modal"
                                    data-target="#modal-label">Add Label</button>


                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="main-box-body clearfix">
                                            <div class="table-responsive clearfix">

                                                <div id="employmentCategoryTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>

                                                            <th>#</th>
                                                            <th><span>Name</span></th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <g:each in="${labels}" status="index"
                                                                var="label">

                                                            <tr><td>
                                                                ${index + 1}
                                                            </td>
                                                                <td id="label_name_${label.uuid}">${label.name ?: '-'}</td>

                                                                <td>
                                                                    <button type="button"
                                                                            class=" btn btn-sm btn-primary"
                                                                            onclick='updateLabelDetail("${label.uuid}")'
                                                                            data-toggle="modal"
                                                                            data-target="#modal-label">Edit</button>


                                                                    <g:link class=" btn btn-sm btn-danger"
                                                                            controller="label"
                                                                            action="deleteLabel"
                                                                            name="labelDeleteLink"
                                                                            params="[id: label.uuid]">Delete</g:link>

                                                                </td>

                                                            </tr>

                                                        </g:each>

                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-1"></div>

</div>
<g:render template="/label/modalForLabel"/>


<script>
    function updateLabelDetail(uuid) {


        var labelName = $('#label_name_' + uuid).html();


        $('#labelName').val(labelName);


        $("#url").val('updateLabel');
        $("#uuid").val('' + uuid);


    }


    function resetLabelDetailsFromModal() {

        $("#labelName").val('');
        $("#url").val('createLabel');


    }

    $(function () {
        $('a[name="labelDeleteLink"]').bind('click', function () {

            if (confirm('Are you Sure?')) {
            }
            else {
                $(this).attr('href', "");
            }
        })
    })


    mixpanel.track("page event", {
        "id": "labelPage",
        "userId":"${user?.id}",
        "first_name": "${user?.firstName}",
        "last_name": "${user?.lastName}",
        "email": "${user?.username}"
    });
    $( "#idDone" ).click(function( event ) {
        debugger;
        mixpanel.track("button event", {
            "id": "labelPage doneButton",
            "userId":"${user?.id}",
            "first_name": "${user?.firstName}",
            "last_name": "${user?.lastName}",
            "email": "${user?.username}"
        });
    });
</script>

</body>
</html>