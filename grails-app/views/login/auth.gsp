%{--<html>--}%
%{--<head>--}%
%{--<meta name='layout' content='main'/>--}%
%{--<title><g:message code="springSecurity.login.title"/></title>--}%
%{--<style type='text/css' media='screen'>--}%
%{--#login {--}%
%{--margin: 15px 0px;--}%
%{--padding: 0px;--}%
%{--text-align: center;--}%
%{--}--}%

%{--#login .inner {--}%
%{--width: 340px;--}%
%{--padding-bottom: 6px;--}%
%{--margin: 60px auto;--}%
%{--text-align: left;--}%
%{--border: 1px solid #aab;--}%
%{--background-color: #f0f0fa;--}%
%{---moz-box-shadow: 2px 2px 2px #eee;--}%
%{---webkit-box-shadow: 2px 2px 2px #eee;--}%
%{---khtml-box-shadow: 2px 2px 2px #eee;--}%
%{--box-shadow: 2px 2px 2px #eee;--}%
%{--}--}%

%{--#login .inner .fheader {--}%
%{--padding: 18px 26px 14px 26px;--}%
%{--background-color: #f7f7ff;--}%
%{--margin: 0px 0 14px 0;--}%
%{--color: #2e3741;--}%
%{--font-size: 18px;--}%
%{--font-weight: bold;--}%
%{--}--}%

%{--#login .inner .cssform p {--}%
%{--clear: left;--}%
%{--margin: 0;--}%
%{--padding: 4px 0 3px 0;--}%
%{--padding-left: 105px;--}%
%{--margin-bottom: 20px;--}%
%{--height: 1%;--}%
%{--}--}%

%{--#login .inner .cssform input[type='text'] {--}%
%{--width: 120px;--}%
%{--}--}%

%{--#login .inner .cssform label {--}%
%{--font-weight: bold;--}%
%{--float: left;--}%
%{--text-align: right;--}%
%{--margin-left: -105px;--}%
%{--width: 110px;--}%
%{--padding-top: 3px;--}%
%{--padding-right: 10px;--}%
%{--}--}%

%{--#login #remember_me_holder {--}%
%{--padding-left: 120px;--}%
%{--}--}%

%{--#login #submit {--}%
%{--margin-left: 15px;--}%
%{--}--}%

%{--#login #remember_me_holder label {--}%
%{--float: none;--}%
%{--margin-left: 0;--}%
%{--text-align: left;--}%
%{--width: 200px--}%
%{--}--}%

%{--#login .inner .login_message {--}%
%{--padding: 6px 25px 20px 25px;--}%
%{--color: #c33;--}%
%{--}--}%

%{--#login .inner .text_ {--}%
%{--width: 120px;--}%
%{--}--}%

%{--#login .inner .chk {--}%
%{--height: 12px;--}%
%{--}--}%
%{--</style>--}%
%{--</head>--}%

%{--<body>--}%
%{--<div id='login'>--}%
%{--<div class='inner'>--}%
%{--<div class='fheader'><g:message code="springSecurity.login.header"/></div>--}%

%{--<g:if test='${flash.message}'>--}%
%{--<div class='login_message'>${flash.message}</div>--}%
%{--</g:if>--}%

%{--<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>--}%
%{--<p>--}%
%{--<label for='username'><g:message code="springSecurity.login.username.label"/>:</label>--}%
%{--<input type='text' class='text_' name='j_username' id='username'/>--}%
%{--</p>--}%
%{----}%
%{--<p>--}%
%{--<label for='password'><g:message code="springSecurity.login.password.label"/>:</label>--}%
%{--<input type='password' class='text_' name='j_password' id='password'/>--}%
%{--</p>--}%
%{----}%
%{--<p id="remember_me_holder">--}%
%{--<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>--}%
%{--<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>--}%
%{--</p>--}%
%{----}%
%{--<p>--}%
%{--<input type='submit' id="submit" value='${message(code: "springSecurity.login.button")}'/>--}%
%{--</p>--}%
%{--</form>--}%
%{--</div>--}%
%{--</div>--}%
%{--<script type='text/javascript'>--}%
%{--<!----}%
%{--(function() {--}%
%{--document.forms['loginForm'].elements['j_username'].focus();--}%
%{--})();--}%
%{--// -->--}%
%{--</script>--}%
%{--</body>--}%
%{--</html>--}%


<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="CleverAdmin - Bootstrap Admin Template">
		<meta name="author" content="Łukasz Holeczek">
		<meta name="keyword" content="CleverAdmin, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="assets/ico/favicon.png">

	    <title>cleverAdmin - Bootstrap Admin Template</title>

	    <!-- Bootstrap core CSS -->
<asset:stylesheet href="css/bootstrap.min.css" />

		<!-- page css files -->
<asset:stylesheet href="css/font-awesome.min.css"/>
<asset:stylesheet href="css/jquery-ui.min.css" />

	    <!-- Custom styles for this template -->
<asset:stylesheet href="css/style.min.css" />

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
</head>

<body>
		<div class="container-fluid content">
		<div class="row">
					<div id="content" class="col-sm-12 full">
			<div class="row">
				<div class="login-box">

					<div class="header">
						Login to Issue Tracker
					</div>


					<div class="text-with-hr">
						<span>your username</span>
					</div>

					<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>

						<fieldset class="col-sm-12">
							<div class="form-group">
							  	<div class="controls row">
									<div class="input-group col-sm-12">
										<input type='text' class='form-control text_' name='j_username' id='username'/>
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
									</div>
							  	</div>
							</div>

							<div class="form-group">
							  	<div class="controls row">
									<div class="input-group col-sm-12">
										<input type='password' class='form-control text_' name='j_password' id='password'/>
										<span class="input-group-addon"><i class="fa fa-key"></i></span>
									</div>
							  	</div>
							</div>

							<div class="confirm">
								<input type="checkbox" name="remember"/>
								<label for="remember">Remember me</label>
							</div>

							<div class="row">
								<input type='submit' class="btn btn-lg btn-primary col-xs-12" id="submit" value='${message(code: "springSecurity.login.button")}'/>
								%{--<button type="submit" class="btn btn-lg btn-primary col-xs-12">Login</button>--}%

							</div>

						</fieldset>

					</form>

					<a class="pull-left" href="page-login.html#">Forgot Password?</a>
					<a class="pull-right" href="page-register.html">Sign Up!</a>

					<div class="clearfix"></div>

				</div>
			</div><!--/row-->

		</div>

				</div><!--/row-->

	</div><!--/container-->


	<!-- start: JavaScript-->
	<!--[if !IE]>-->

<asset:javascript src="js/jquery-3.1.0.min.js"/>

	<!--<![endif]-->

	<!--[if IE]>

<asset:javascript src="js/jquery-1.11.0.min.js"/>

	<![endif]-->

	<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-3.1.0.min.js'>"+"<"+"/script>");
		</script>

	<!--<![endif]-->

	<!--[if IE]>

		<script type="text/javascript">
	 	window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
		</script>

	<![endif]-->
	%{--<script src="assets/js/jquery-migrate-1.4.1.min.js"></script>--}%
<asset:javascript src="js/bootstrap.min.js"/>

<asset:javascript src="js/jquery-migrate-1.4.1.min.js"/>


<!-- page scripts -->
<asset:javascript src="js/jquery.icheck.min.js"/>

<!-- theme scripts -->
	<asset:javascript src="js/custom.min.js"/>
		<asset:javascript src="js/core.min.js"/>

<!-- inline scripts related to this page -->
<asset:javascript src="js/pages/login.js"/>

<!-- end: JavaScript-->

</body>
</html>