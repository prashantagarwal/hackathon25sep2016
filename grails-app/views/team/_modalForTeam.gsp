<div class="modal fade" id="modal-label" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Team</h4>
            </div>

        <div class="modal-body">
            <g:form id="labelForm" controller="team" action="createUpdateTeam">
                <div class="form-group">
                    <label for="labelName">Team Name</label>
                    <input type="text" class="form-control" id="labelName" name="teamName"
                           placeholder="Team Name">
                </div>

                <div class="form-group">
                    <label for="labelUser">Select User</label>
                    <g:select noSelection="['': 'Select']" id="labelUser"
                              class="form-control userDropdown" from="${nextho.AppUtil.userList()}"
                              name="tag"
                              optionKey="emailAddress"
                              optionValue="emailAddress"
                              tabindex="-1"

                    />
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label for="users">Users</label>
                        <textarea class="form-control" style="font-weight: bold" rows="5" id="users" name="allUsers"></textarea>
                    </div>
                </div>

                </div>
                <input type="hidden" name="url" id="url"/>
                <input type="hidden" name="uuid" id="uuid"/>


                <div class="modal-footer">
                    <button type="submit" id="" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </g:form>

        </div>

    </div>
</div>




<script>
    function resetUpdateValue() {
        $('#modalwindow').modal('hide');
    }
</script>


