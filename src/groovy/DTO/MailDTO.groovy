package DTO

/**
 * Created by yogesh on 24/9/16.
 */
class MailDTO {
    String to
    String body
    String role
    String from
    String teamName
}
