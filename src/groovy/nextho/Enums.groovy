package nextho

class Enums {

    public enum Access {
        READ, WRITE, READ_WRITE
    }

    public enum NotificationQueueStatus {
        FAILED, SENT, QUEUE
    }
}
