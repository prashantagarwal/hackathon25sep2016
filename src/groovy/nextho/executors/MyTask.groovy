package nextho.executors

import java.util.concurrent.TimeUnit


class MyTask implements Runnable {

    private String name

    public MyTask(String name)
    {
        this.name = name
    }

    public String getName() {
        return name
    }

    @Override
    public void run()
    {
        try
        {
            Long duration = 5l
            println("Doing a task during : " + name + " - Time - " + new Date())

        }
        catch (InterruptedException e)
        {
            e.printStackTrace()
        }
    }


}
