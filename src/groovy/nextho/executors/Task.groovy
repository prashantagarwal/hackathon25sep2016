package nextho.executors

import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes as GA

class Task implements Runnable
{

    def ctx = SCH.servletContext.getAttribute(GA.APPLICATION_CONTEXT)
    def taskExecutor = ctx.taskExecutorService

    private String name;

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run()
    {
        try {
            println("########################## Running Sceduler" + name + " - Time - " + new Date())
            taskExecutor.fetchNotificationQueuesAndSendMail()

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}