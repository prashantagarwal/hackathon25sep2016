package nextho


class Email {


    public static String sendEmail(String sentTo){
        HttpURLConnection con = null;
        OutputStreamWriter writer=null;
        String inputData=" {\n" +
                "  \"From\": \"hello@p2pforce.com\",\n" +
                "  \"To\": \""+sentTo+"\",\n" +
                "  \"Bcc\": \"blank-copied@example.com\",\n" +
                "  \"Subject\": \"Test\",\n" +
                "  \"Tag\": \"Invitation\",\n" +
                "  \"HtmlBody\": \"<b>Hello</b>\",\n" +
                "  \"TextBody\": \"Hello\",\n" +
                "  \"ReplyTo\": \"reply@example.com\",\n" +
                "  \"Headers\": [\n" +
                "    { \n" +
                "      \"Name\": \"CUSTOM-HEADER\", \n" +
                "      \"Value\": \"value\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"TrackOpens\": true\n" +
                "}"
        String googleVisionURL="https://api.postmarkapp.com/email"
        String completeURL=googleVisionURL
        URL obj = new URL(completeURL)
        con = (HttpURLConnection) obj.openConnection()
        con.setDoInput(true)
        con.setDoOutput(true)
        con.setRequestMethod("POST")
        con.setRequestProperty("Accept", "application/json")
        con.setRequestProperty("Content-Type","application/json")
        con.setRequestProperty("X-Postmark-Server-Token","61eded54-7fbd-4b94-aee8-2a5b4565d336")
        writer = new OutputStreamWriter(con.getOutputStream(),"UTF-8")
        writer.write(inputData)
        writer.flush()
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer responseStream =new StringBuffer();
        while((inputLine= bufferedReader.readLine())!=null)
        {
            responseStream.append(inputLine);
        }
        String apiResponse = responseStream.toString();
        return apiResponse
    }
}
