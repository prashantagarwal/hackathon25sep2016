package nextho

/**
 * Created by chetan on 24/9/16.
 */
public enum Roles {

    ROLE_ADMIN('ROLE_ADMIN', 'Administrator'),
    ROLE_DEV('ROLE_AGENT', 'Developer'),
//    ROLE_USER('ROLE_USER', 'USER'),



    def value
    def description

    public Roles(value, description) {
        this.value = value
        this.description = description
    }



}


